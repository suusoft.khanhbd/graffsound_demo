const state = () => ({
  music: {},
  playState: {},
  playlist: []
});

const getters = {};

const actions = {
  playMusic({ commit }, data) {
    commit("setPlayMusic", data);
    localStorage.setItem("music", JSON.stringify(data));
    return true;
  }
};

// mutations
const mutations = {
  setPlayMusic(state, data) {
    state.music = data;
  },
  setPlaylist(state, data) {
    state.playlist = data;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
