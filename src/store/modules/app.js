const state = () => ({
  actionRedirect: {
    type: null
  },
  listGenre: [],
  listArtist: {},
  initialData: [],
  user: {
    logged: false
  }
});

// Mutations
const mutations = {
  setActionRedirect(state, data) {
    state.actionRedirect = data;
  },
  setListGenre(state, data) {
    state.listGenre = data;
  },
  setListArtist(state, data) {
    state.listArtist = data;
  },
  setInitialData(state, data) {
    state.initialData = data;
  },
  setUser(state, data) {
    state.user = data;
  }
};

export default {
  namespaced: true,
  state,
  mutations
};
