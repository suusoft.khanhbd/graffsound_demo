import Vue from 'vue'
import Vuex from 'vuex'
import player from './modules/player'
import app from './modules/app'


Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    player,
    app
  },
  strict: debug
})
