import Vue from 'vue'
import Router from 'vue-router'

// Page content
import Home from '@/components/Home'
import Mood from '@/components/Mood'
import Chart from '@/components/Chart'
import Release from '@/components/Release'
import PaidItem from '@/components/PaidItem'
import Song from '@/components/Song'
import Beat from '@/components/Beat'
import BGM from '@/components/BGM'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Detail from '@/components/Detail'

import GoFundMe from '@/components/GoFundMe'
import FundDetail from '@/components/FundDetail'

import Voting from '@/components/Voting'
import VotingChart from '@/components/VotingChart'

import Shopping from '@/components/shopping/Shopping'
import Products from '@/components/shopping/Products'
import Checkout from '@/components/shopping/Checkout'

// Fallback page
import PageNotFound from '@/components/PageNotFound'
import ProductDetail from "@/components/shopping/ProductDetail";

import GSMT from '@/components/GSMT'

// Profile
import  Profile from '@/components/Profile'
import  EditProfile from '@/components/EditProfile'

// Payment
import  Payment from '@/components/Payment'

// Fallback page
import FundDetail_OnGoing from '@/components/FundDetail_OnGoing'
import Category from '@/components/Category'

// Add artists
import ArtistsIndex from '@/components/artists/Index'
import ArtistsDetail from '@/components/artists/Detail'

// Search
import Search from '@/components/Search'

// Artist-registration
import ArtistRegister from '@/components/artist-profile/Register'

// Artist-profile
import ArtistProfile from '@/components/artist-profile/Index'

// Upload
import UploadMusic from '@/components/upload/UploadMusic'

// Update password
import UpdatePassword from '@/components/UpdatePassword'

import Recent from  '@/components/Recent'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },

    {
      path: '/category',
      name: 'Category',
      component: Category
    },

    {
      path: '/detail/:id',
      name: 'Detail',
      component: Detail
    },

    {
      path: '/Mood',
      name: 'Mood',
      component: Mood
    },

    {
      path: '/Chart',
      name: 'Chart',
      component: Chart
    },

    {
      path: '/Release',
      name: 'Release',
      component: Release
    },

    {
      path: '/sound',
      name: 'Paid',
      component: PaidItem
    },

    {
      path: '/song',
      name: 'Song',
      component: Song
    },

    {
      path: '/beat',
      name: 'Beat',
      component: Beat
    },

    {
      path: '/bgm',
      name: 'BGM',
      component: BGM
    },

    {
      path: '/gsmt',
      name: 'gsmt',
      component: GSMT
    },

    {
      path: '/login',
      name: 'Login',
      component: Login
    },

    {
      path: '/register',
      name: 'Register',
      component: Register
    },

    {

      path: "/funddetail-ongoing",
      name: "funddetail-ongoing",
      component: FundDetail_OnGoing
    },

    {
      path: "/gofundme",
      name: "GoFundMe",
      component: GoFundMe
    },

    {
      path: "/funddetail",
      name: "Fund Detail",
      component: FundDetail
    },
    {
      path: '/voting',
      name: 'Voting',
      component: Voting
    },

    {
      path: '/voting-chart',
      name: 'VotingChart',
      component: VotingChart
    },

    {
      path: '/shopping',
      name: 'Shopping',
      component: Shopping

    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/edit-profile',
      name: 'EditProfile',
      component: EditProfile
    },
    {
      path: '/update-password',
      name: 'UpdatePassword',
      component: UpdatePassword
    },
    {
      path: '/payment',
      name: 'Payment',
      component: Payment
    },
    {
      path: '/shopping/category/:id',
      name: 'Category',
      component: Products
    },

    {
      path: '/shopping/products',
      name: 'Products',
      component: Products
    },

    {
      path: '/shopping/product/:id',
      name: 'ProductDetail',
      component: ProductDetail
    },

    {
      path: '/shopping/checkout',
      name: 'Checkout',
      component: Checkout
    },
    {
      path: '/artists',
      name: 'artists',
      component: ArtistsIndex
    },
    {
      path: '/artists/detail/:id',
      name: 'artists_detail',
      component: ArtistsDetail
    },
    {
      path: '/recent',
      name: 'recent',
      component: Recent
    },
    {
      path: '/search/',
      name: 'search',
      component: Search
    },
    {
      path: '/artist-registration',
      name: 'artist-registration',
      component: ArtistRegister
    },
    {
      path: '/artist-profile',
      name: 'artist-profile',
      component: ArtistProfile
    },
    {
      path: '/upload-music',
      name: 'upload-music',
      component: UploadMusic
    },
    {
      path: '**',
      name: 'PageNotFound',
      component: PageNotFound
    },
  ]
})
