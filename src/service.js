import axios from "axios";

const Base_URL = "https://suusoft-demo.com/projects/grafsound-music-web/backend/web/index.php/api/";

const defaultConfig = {
  baseURL: Base_URL
};

const commonAxios = axios.create(defaultConfig);

const get = async (url, data = {}) => {
  try {
    const Authorization = localStorage.getItem("Authorization");
    const header = {
      headers: {
        Authorization
      }
    }
    if (!Authorization) delete header.headers.Authorization;

    const res = await commonAxios.get(url, data, header);

    return res;
  } catch (error) {
    console.log("service error", error);
  }
};

const post = async (url, body = {}) => {
  try {
    const Authorization = localStorage.getItem("Authorization");
    const header = {
      headers: {
        Authorization
      }
    }
    if (!Authorization) delete header.headers.Authorization;

    const res = await commonAxios.post(url, body, header);

    return res;
  } catch (error) {
    console.log("service error", error);
  }
};

export default { get, post };
