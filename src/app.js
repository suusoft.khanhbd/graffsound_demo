import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import VueRouter from 'vue-router'
import VModal from 'vue-js-modal/dist/index.nocss.js'
import 'vue-js-modal/dist/styles.css'
import Vuex from 'vuex'
import store from './store'

Vue.config.productionTip = false;

if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('/service-worker-cache.js')
    .then(() => {
      console.log('Service Worker Registered')
    })
}
Vue.use(Vuex)
Vue.use(VModal)
Vue.use(Antd)
Vue.use(VueRouter)

Vue.mixin({
  methods: {
    isMobileGlobal() {
      if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return true;
      } else {
        return false;
      }
    },
    formatterTimeGl(value) {
      let minutes = Math.floor(value / 60);
      let s = Math.round(value % 60);
      let str = "";
      if (minutes < 10) {
        str = "0" + minutes;
      } else {
        str = minutes + "";
      }
      if (s < 10) {
        str += ":" + "0" + s;
      } else {
        str += ":" + s;
      }
      return str;
    }
  }
})

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});
